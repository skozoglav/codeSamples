.equ PMC_BASE,          0xFFFFFC00          /* (PMC) Base Address */
.equ CKGR_MOR,	        0x20                /* (CKGR) Main Oscillator Register */
.equ CKGR_PLLAR,        0x28                /* (CKGR) PLL A Register */
.equ PMC_MCKR,          0x30                /* (PMC) Master Clock Register */
.equ PMC_SR,            0x68                /* (PMC) Status Register */
.equ PMC_PCER,          0x10  

.equ PIOC_BASE,         0xFFFFF800  /* Začetni naslov registrov za PIOB */
.equ PIO_PER,           0x00	/* Odmiki... */
.equ PIO_OER,           0x10
.equ PIO_SODR,          0x30
.equ PIO_CODR,          0x34


.equ TC0_BASE,          0xFFFA0000              /* TC0 Channel Registers */
.equ TC_IMR,            0x02C                   /* TC0 Interrupt Mask Register */
.equ TC_IER,            0x24                    /* TC0 Interrupt Enable Register*/
.equ TC_RC,             0x1C                    /* TC0 Register C */
.equ TC_RA,             0x14                    /* TC0 Register A */
.equ TC_CMR,            0x04                    /* TC0 Channel Mode Register (Capture Mode / Waveform Mode */
.equ TC_IDR,            0x28                    /* TC0 Interrupt Disable Register */
.equ TC_SR,             0x20                    /* TC0 Status Register */
.equ TC_RB,             0x18                    /* TC0 Register B */
.equ TC_CV,             0x10                    /* TC0 Counter Value */
.equ TC_CCR,            0x00                    /* TC0 Channel Control Register */

.text
.code 32

.global _error
_error:
  b _error

.global	_start
_start:

/* select system mode 
  CPSR[4:0]	Mode
  --------------
   10000	  User
   10001	  FIQ
   10010	  IRQ
   10011	  SVC
   10111	  Abort
   11011	  Undef
   11111	  System   
*/

  mrs r0, cpsr
  bic r0, r0, #0x1F   /* clear mode flags */  
  orr r0, r0, #0xDF   /* set supervisor mode + DISABLE IRQ, FIQ*/
  msr cpsr, r0     
  
  /* init stack */
  ldr sp,_Lstack_end
                                   
  /* setup system clocks */
  ldr r1, =PMC_BASE

  ldr r0, = 0x0F01
  str r0, [r1,#CKGR_MOR]
                       

osc_lp:
  ldr r0, [r1,#PMC_SR]
  tst r0, #0x01
  beq osc_lp
  
  mov r0, #0x01
  str r0, [r1,#PMC_MCKR]

  ldr r0, =0x2000bf00 | ( 124 << 16) | 12       /* 18,432 MHz * 125 / 12 */
  str r0, [r1,#CKGR_PLLAR]

pll_lp:
  ldr r0, [r1,#PMC_SR]
  tst r0, #0x02
  beq pll_lp

  /* MCK = PCK/4 */
  ldr r0, =0x0202
  str r0, [r1,#PMC_MCKR]

mck_lp:
  ldr r0, [r1,#PMC_SR]
  tst r0, #0x08
  beq mck_lp

  /* Enable caches */
  mrc p15, 0, r0, c1, c0, 0 
  orr r0, r0, #(0x1 <<12) 
  orr r0, r0, #(0x1 <<2)
  mcr p15, 0, r0, c1, c0, 0 

.global _main
/* main program */
_main:

/* user code here */
        bl INIT_PIO      
        bl INIT_TC0
        bl LED_OFF
        
        adr r5, beseda
        mov r9, #6
        
loop:   bl XWORD      
        b loop
      

/* end user code */

_wait_for_ever:
  b _wait_for_ever

/* Inicializacija PIOC registra */

INIT_PIO:   stmfd r13!, {r1, r2, r14}
            ldr r2, =PIOC_BASE
            mov r1, #1 << 1
            
            str r1, [r2, #PIO_PER]
            str r1, [r2, #PIO_OER]           
            
            ldmfd r13!, {r1, r2, pc}
            
/* Prižig LED diode */
LED_ON:     stmfd r13!, {r1, r2, r14}
            ldr r2, =PIOC_BASE
            mov r1, #1 << 1
            
            str r1, [r2, #PIO_CODR]
            ldmfd r13!, {r1, r2, pc}

/* Izklop LED diode */
LED_OFF:    stmfd r13!, {r1, r2, r14}
            ldr r2, =PIOC_BASE
            mov r1, #1 << 1
            
            str r1, [r2, #PIO_SODR]
            ldmfd r13!, {r1, r2, pc}
            
/* Inicializacija časovnika */
INIT_TC0:   stmfd r13!, {r1, r2, r14}
            ldr r2, =PMC_BASE
            
            mov r1, #1 << 17
            str r1, [r2, #PMC_PCER]
            
            ldr r2, =TC0_BASE
            
            mov r1, #0b110 << 13
            add r1, r1, #0b11
            str r1, [r2, #TC_CMR]                    
            
            ldr r1, =375
            str r1, [r2, #TC_RC]
            
            mov r1, #0b101
            str r1, [r2, #TC_CCR]

            ldmfd r13!, {r1, r2, pc}

/* čakanje na zastavico */
DELAY:      stmfd r13!, {r1, r2, r14}
            ldr r2, =TC0_BASE
            
DLY:        ldr r1, [r2, #TC_SR]
            tst r1, #1 << 4
            beq DLY
            
            subs r6, r6, #1
            bne DLY

            ldmfd r13!, {r1, r2, pc}
 
/* prižig led diode glede na parameter v registru r0 */ 
XMCHAR:     stmfd r13!, {r14}     
            cmp r0, #'.'
            ldreq r6, =150            
            cmp r0, #'-'
            ldreq r6, =300
            
            bl LED_ON
            bl DELAY
            
            ldr r6, =150
            bl LED_OFF
            bl DELAY
            
            ldmfd r13!, {pc}
            
/* podprogram gre skozi niz, na katerega kaže r0    */
/* niz je zaključen z 0 in vsebuje samo '.' in '-'  */
/* za vsak znak prizgi LED diodo za določen cas     */

XMCODE:     stmfd r13!, {r3, r4, r14}
            
            mov r4, r0    

CODEL:      ldrb r3, [r4], #1  
            cmp r3, #'0'
            beq konec         
            mov r0, r3
            bl XMCHAR
            b CODEL

konec:      ldr r6, =300   
            bl DELAY
            ldmfd r13!, {r3, r4, pc}
            
/* podprogram v r0 dobi ASCII kodo velike tiskane črke A-Z     */
/* podprogram v r0 vrne kazalec na z niclo zakljucen niz       */
/* podprogram uporablja tabelo, v kateri so morsejeve kode crk */
/* vsaka crka je predstavljena s sestimi znaki                 */
GETMCODE:   stmfd r13!, {r10, r14}
            adr r0, niz 
            mov r10, r0
            subs r8, r8, #65
            mul r8, r8, r9
            add r1, r0, r8    
            mov r0, r1
            bl XMCODE

            ldmfd r13, {r10, pc}
            
/* podprogram v r0 dobi kazalec na z niclo zakljucen niz v katerem so samo velike crke         */
/* podprogram po morsejevi kodi posilja crko po crko, nato pocaka 1 sekundo preden se zakljuci */
XWORD:    stmfd r13!, {r7, r14}
          
          @mov r7, r5
          ldr r7, =beseda
          
xloop:    ldrb r8, [r7], #1
          cmp r8, #0
          beq xend
          bl GETMCODE
          b xloop          

xend:     ldr r6, =1000
          bl DELAY
          
          ldmfd r13!, {r7, pc}
            
/* constants */

niz:  .ascii ".-0000-...00-.-.00-..000.00000..-.00--.000....00..0000.---00-.-000.-..00--0000-.0000---000.--.00--.-00.-.000...000-00000..-000...-00.--000-..-00-.--00--..00"

beseda:   .asciz "SOS"

