#include <stdio.h>
#include <omp.h>
#include <time.h>
#include <math.h>

// interval [a, b]
#define a 1		
#define b 100000

// spremenljivke uporabljene za merjenje casa
clock_t start, stop;
double s_time, p_time;

int parallel(int NTHREADS);
int getDiv(int n);

void main() {
	
	// klici funkcijo z eno nitjo in meri njen cas delovanja
	start = clock();
	int sum = parallel(1);
	stop = clock();
	s_time = (double)(stop - start) / CLOCKS_PER_SEC;

	// klici funkcijo s štirimi nitmi in meri njen cas delovanja
	start = clock();
	parallel(4);
	stop = clock();
	p_time = (double)(stop - start) / CLOCKS_PER_SEC;

	// izpišemo rezultat funkcij in njihov cas delovanja
	printf("\nVsota: %d\n", sum);
	printf("Cas porabljen za izvajanje programa na eni niti: %.3f\n", s_time);
	printf("Cas porabljen za izvajanje programa na stirih nitih: %.3f\n\n", p_time);
	printf("Pohitritev programa: %.3f\n\n", s_time / p_time);
}

int parallel(int NTHREADS) {
	
	// inicializacija spremenljivk
	int i, temp, sum = 0;

	// nastavi število niti
	omp_set_num_threads(NTHREADS);

	// paralelno cez for
	// private(temp)	-> vsaka nit ima svojo kopijo spremenljivke temp da ne pride do napacnega rezultata
	// reduction(+:sum) -> vsaka nit ima svojo kopijo spremenljivke sum, na koncu se kopije združene s seštevanjem
	#pragma omp parallel for reduction(+:sum) private(temp) schedule(dynamic, 10)
	for (i = a; i < b; i++) {
		temp = getDiv(i);
		if (temp > i && getDiv(temp) == i) {
			sum += (i + temp);
		}
	}

	return sum;
}

int getDiv(int n) {

	int t = 0;				// vsota deliteljev števila n
	int d = (int) sqrt(n);	// limit do kjer se zanka izvaja

	for (int i = 1; i <= d; i++) {
		// ce je n deljiv z i, naši vsoti deliteljev prištejemo i
		if (n%i == 0) {
			t += i;
			// da pohitrimo iskanje deliteljev lahko prištejemo tudi njegov "nasprotni" delitelj
			// postaviti moramo pogoj da ni število enako, da ne štejemo dvakrat enega delitelja, in pa samega sebe
			if (i != n / i && i != 1) {
				t += n / i;
			}
		}
	}
	
	// vrne vsoto deliteljev števila n
	return t;
}
