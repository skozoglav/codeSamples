## Code samples

#### C programming language
Quick solution to sum pairs of amicable numbers on certain interval [a, b] using OpenMP.

#### Assembly programming language
Morse code program using FRI-SMS board. Program demonstrates morse code with blinking LED.
